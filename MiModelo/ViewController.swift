//
//  ViewController.swift
//  MiModelo
//
//  Created by Jesús Gallego Irles on 24/2/17.
//  Copyright © 2017 Jesús Gallego Irles. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        // Crear propietario
        let propietario = NSEntityDescription.insertNewObject(forEntityName: "Usuario", into: context) as! Usuario
        propietario.nombre = "Jesus"
        propietario.edad = 22
        
        // Crear habitaciones
        let hab1 = NSEntityDescription.insertNewObject(forEntityName: "Habitacion", into: context) as! Habitacion
        hab1.metros = 12.5
        let hab2 = NSEntityDescription.insertNewObject(forEntityName: "Habitacion", into: context) as! Habitacion
        hab2.metros = 16.2
        
        // Crear casa
        let casa = NSEntityDescription.insertNewObject(forEntityName: "Casa", into: context) as! Casa
        casa.calle = "Calle falsa"
        casa.numero = 123
        casa.propietario = propietario
        casa.addToHabitaciones(hab1)
        casa.addToHabitaciones(hab2)
        do {
            try context.save()
            print("Modelos creados correctamente")
            
            print("Propietario de la casa en \(propietario.casa!.calle!)")
            print("Habitacion 1 de la casa en \(hab1.casa!.calle!)")
            print("Habitacion 2 de la casa en \(hab2.casa!.calle!)")
            
        } catch {
            print(error.localizedDescription)
        }
        
        // Borrar en cascada
        context.delete(casa)
        do {
            try context.save()
            print("casa borrada correctamente")
            
            context.processPendingChanges()
            
            let request: NSFetchRequest<Habitacion> = NSFetchRequest(entityName: "Habitacion")
            let habitaciones = try! context.fetch(request)
            print("Numero de habitaciones: \(habitaciones.count)")
        } catch {
            print(error.localizedDescription)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

